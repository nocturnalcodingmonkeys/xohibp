#tag Class
Protected Class XoHIBP
Inherits Xojo.Net.HTTPSocket
	#tag Event
		Sub Error(err as RuntimeException)
		  self.mIsError   = true
		  self.merrorCode = cstr( err.ErrorNumber ) + EndOfLine + err.Message + EndOfLine + err.Reason + EndOfLine + join( err.Stack, EndOfLine )
		  
		  #IF DebugBuild THEN System.DebugLog self.errorCode
		End Sub
	#tag EndEvent

	#tag Event
		Sub PageReceived(URL as Text, HTTPStatus as Integer, Content as xojo.Core.MemoryBlock)
		  self.parse_PageReceivedEvent( URL,HTTPStatus,Content )
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Function convertToMemorBlock(m as Xojo.Core.MemoryBlock) As MemoryBlock
		  dim mbTemp   as MemoryBlock = m.data
		  dim mbRetVal as new MemoryBlock( m.size )
		  mbRetVal.StringValue( 0,mbRetVal.Size ) = mbTemp.StringValue( 0, mbRetVal.size )
		  mbTemp = NIL
		  return mbRetVal
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub fetchBreachedAccount(emailAddress as string)
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchBreachedAccount( " + emailAddress + " ) started"
		  if emailAddress.trim.Len < kEmailAdressMinimum then return 
		  if UserAgent.trim.len < kUserAgentMinimum      then return 
		  self.socktHasBeenReset = false
		  
		  dim txtURL as text = self.kURL_BreachedAccount + emailAddress.trim.ToText
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchBreachedAccount( " + emailAddress + " ) " + txtURL
		  
		  self.RequestHeader( "api-version" ) = kHeader_APIVersion.ToText
		  self.RequestHeader( "Accept"      ) = kHeader_Accept.ToText
		  self.RequestHeader( "User-Agent"  ) = UserAgent.ToText
		  
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchBreachedAccount( " + emailAddress + " ) GET"
		  
		  Send( "GET",txtURL )
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchBreachedAccount( " + emailAddress + " ) ended"
		  self.mIsError   = false
		  self.merrorCode = ""
		  self.mstatus    = ""
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub fetchPastedAccount(emailAddress as string)
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchPastedAccount( " + emailAddress + " ) started"
		  if emailAddress.trim.Len < kEmailAdressMinimum then return 
		  if UserAgent.trim.len < kUserAgentMinimum      then return 
		  self.socktHasBeenReset = false
		  
		  dim txtURL as text = self.kURL_PastedAccount + emailAddress.trim.ToText
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchPastedAccount( " + emailAddress + " ) " + txtURL
		  
		  self.RequestHeader( "api-version" ) = kHeader_APIVersion.ToText
		  self.RequestHeader( "Accept"      ) = kHeader_Accept.ToText
		  self.RequestHeader( "User-Agent"  ) = UserAgent.ToText
		  
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchPastedAccount( " + emailAddress + " ) GET"
		  
		  Send( "GET",txtURL )
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchPastedAccount( " + emailAddress + " ) ended"
		  self.mIsError   = false
		  self.merrorCode = ""
		  self.mstatus    = ""
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub fetchPwnedPassword(password as string)
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchPwnedPassword( " + password + " ) started"
		  if password.trim.Len < kEmailAdressMinimum then return 
		  if UserAgent.trim.len < kUserAgentMinimum  then return 
		  self.socktHasBeenReset = false
		  
		  self.passwordToBeFound = passwordSHA1( password.Trim )
		  
		  dim txtURL as text = self.kURL_PwnedPassword + self.passwordToBeFound.ToText.left( 5 )
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchPwnedPassword( " + password + " ) " + txtURL
		  
		  self.RequestHeader( "api-version" ) = kHeader_APIVersion.ToText
		  self.RequestHeader( "Accept"      ) = kHeader_Accept.ToText
		  self.RequestHeader( "User-Agent"  ) = UserAgent.ToText
		  
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchPwnedPassword( " + password + " ) GET"
		  
		  Send( "GET",txtURL )
		  #IF DebugBuild THEN System.DebugLog "XoHIBP.fetchPwnedPassword( " + password + " ) ended"
		  self.mIsError   = false
		  self.merrorCode = ""
		  self.mstatus    = ""
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Attributes( Deprecated ) Private Sub parse_PageReceivedEvent(URL as text, HTTPStatus as integer, Content as xojo.core.memoryblock)
		  #IF DebugBuild THEN System.DebugLog cstr( HTTPStatus )
		  #IF DebugBuild THEN System.DebugLog URL
		  // this is to clear out anything that is in the ethers after we have done a reset.
		  if self.socktHasBeenReset then return
		  
		  if URL.IndexOf( 0,"/Error/" ) > 0 then
		    returnedData = ""
		    MsgBox "ERROR: " + cstr( HTTPStatus ) + EndOfLine + "have an error page."
		  elseif HTTPStatus = 200 then 
		    dim strReturnedData as string = Xojo.Core.TextEncoding.UTF8.ConvertDataToText( Content ) 
		    if strcomp( URL.Lowercase.left( self.kURL_PwnedPassword.Length ),self.kURL_PwnedPassword.Lowercase,0 ) = 0 then
		      dim strLeft as string = self.passwordToBeFound.left( 5 ).Lowercase
		      dim strHashes() as string
		      for each strRight as string in strReturnedData.Split( EndOfLine )
		        strHashes.Append strLeft + strRight.NthField( ":",1 ).Lowercase
		      next
		      System.DebugLog join( strHashes, EndOfLine )
		      if strHashes.IndexOf( self.passwordToBeFound.Lowercase ) > -1 then
		        self.mstatus      = "FOUND"
		        self.returnedData = "FOUND"
		      else
		        self.mstatus      = "NOT FOUND"
		        self.returnedData = "NOT FOUND"
		      end
		      self.passwordToBeFound = ""
		      
		    elseif strcomp( URL.Lowercase.left( self.kURL_BreachedAccount.Length ),self.kURL_BreachedAccount.Lowercase,0 ) = 0 then
		      self.mstatus      = "BREACHED"
		      self.returnedData = "BREACHED" + EndOfLine + strReturnedData
		    else
		      self.mstatus      = "UNKNOWN"
		      self.returnedData = strReturnedData
		    end if
		    
		  else 
		    returnedData = ""
		    MsgBox "ERROR: " + cstr( HTTPStatus ) + EndOfLine + "cant finish the transaction"
		  end if
		  #IF DebugBuild THEN System.DebugLog returnedData
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Function passwordSHA1(password as string) As string
		  dim p as string = password.DefineEncoding( Encodings.UTF8 )
		  return  EncodeHex( Crypto.SHA1( p ) )
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub reset()
		  self.socktHasBeenReset = true
		  self.ClearRequestHeaders
		  self.merrorCode        = ""
		  self.mIsError          = false
		  self.mstatus           = ""
		  self.returnedData      = ""
		  self.passwordToBeFound = ""
		  
		  // NCM: need to see if we can use disconnect() then somehow rebuild ourselves within the object.
		  //      or something similar so can we can reset long running processes or fetches that we dont want
		  //      any more.
		End Sub
	#tag EndMethod


	#tag Note, Name = revisions
		
		20180801a - .trim'd inputs to make sure we dont have lots of spaces/tabs floating around.
		20180801 - addred XoHIBP.reset
		20180323 - initial release
		
	#tag EndNote

	#tag Note, Name = todo
		
		Once I get a free moment I need to backport some of the custom code from the client (with their permission) to the community code base.
		
		need to add more error checking.
		
	#tag EndNote


	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return merrorCode
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'merrorCode = value
			  
			  // READY ONLY VARIABLE
			End Set
		#tag EndSetter
		errorCode As string
	#tag EndComputedProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mIsError
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mIsError = value
			  
			  // READ ONLY VARIABLE
			End Set
		#tag EndSetter
		IsError As boolean
	#tag EndComputedProperty

	#tag Property, Flags = &h21
		Private merrorCode As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mIsError As boolean = false
	#tag EndProperty

	#tag Property, Flags = &h21
		Private mstatus As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private passwordToBeFound As string
	#tag EndProperty

	#tag Property, Flags = &h0
		returnedData As string
	#tag EndProperty

	#tag Property, Flags = &h21
		Private socktHasBeenReset As boolean = false
	#tag EndProperty

	#tag ComputedProperty, Flags = &h0
		#tag Getter
			Get
			  return mstatus
			End Get
		#tag EndGetter
		#tag Setter
			Set
			  'mstatus = value
			  
			  // READ ONLY VARIABLE
			End Set
		#tag EndSetter
		status As string
	#tag EndComputedProperty

	#tag Property, Flags = &h0
		UserAgent As string
	#tag EndProperty


	#tag Constant, Name = kEmailAdressMinimum, Type = Double, Dynamic = False, Default = \"5", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kHeader_Accept, Type = String, Dynamic = False, Default = \"application/vnd.haveibeenpwned.v2+json", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kHeader_APIVersion, Type = String, Dynamic = False, Default = \"2", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kHIBPversion, Type = String, Dynamic = False, Default = \"20180801a", Scope = Public
	#tag EndConstant

	#tag Constant, Name = kURL_BreachedAccount, Type = Text, Dynamic = False, Default = \"https://haveibeenpwned.com/api/v2/breachedaccount/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kURL_PastedAccount, Type = Text, Dynamic = False, Default = \"https://haveibeenpwned.com/api/v2/pasteaccount/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kURL_PwnedPassword, Type = Text, Dynamic = False, Default = \"https://api.pwnedpasswords.com/range/", Scope = Private
	#tag EndConstant

	#tag Constant, Name = kUserAgentMinimum, Type = Double, Dynamic = False, Default = \"5", Scope = Private
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ValidateCertificates"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="UserAgent"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="returnedData"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="IsError"
			Group="Behavior"
			Type="boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="errorCode"
			Group="Behavior"
			Type="string"
		#tag EndViewProperty
		#tag ViewProperty
			Name="status"
			Group="Behavior"
			Type="string"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass
